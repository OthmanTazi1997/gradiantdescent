import matplotlib.pyplot as plt
import numpy as np


#our gradiant descent function it calculate the minimum global
def gradient_descent(function, start, learn_rate, n_iter=50, tolerance=1e-06):
    
    '''
    this function approches the 'x' value for the minimum global of your function 'f(x)' 

    function   : the function that you whant to look for it global minimum          (type : lambda)
    start      : a point (can be random) , where the you start the gradient_descent (type : int or float)
    learn_rate : the step (type : int or float)

    return => float

    '''

    v = start
    for _ in range(n_iter):
        diff = -learn_rate * function(v)
        if np.all(np.abs(diff) <= tolerance):
            break
        v += diff
    return v

# x
xpts = np.linspace(0, 20,100)
#our function f(x)
test = lambda a : (np.sin(a)/a) * (np.exp(-a/100))

#x min global
min = gradient_descent(test , 5 , 0.01)

#ploting the figurs
plt.plot(xpts, test(xpts))
plt.scatter(min , test(min), c='r',  marker='x', s=25)
plt.show()




